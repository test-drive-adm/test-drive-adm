#!/usr/bin/env python3
from datetime import datetime, timedelta

terminate_time = (datetime.now() + timedelta(hours = 3)).strftime('%I:00 %p')
print terminate_time[1:] if '0' == terminate_time[0] else terminate_time
