def supportedVersions = [
  18.4,
  18.3,
  18.2,
  18.1,
  17.4,
  17.3,
  17.2,
  17.1,
  16.3
]
supportedVersions.push("internal-release")
def branch = Thread.currentThread().executable.buildVariables['BRANCH']
def jobList = []
def jobBaseName = 'FitNesse-Pipeline-Job'

supportedVersions.each { version ->
  jobList.add([jobBaseName + '-' + version, String.valueOf(version)])
}

jobList.each { jobName, version ->
  createFitNessePipelineJob(jobName, branch, version)
}

jobList.each { jobName, version ->
  queue(jobName)
}

listView('FitNesse-for-Appian-Automated-Tests') {
  jobs {
    names(
      JOB_NAME,
      *jobList.collect { it[0] }
    )
  }
  columns {
    status()
    weather()
    name()
    lastSuccess()
    lastFailure()
    lastDuration()
    buildButton()
  }
  configure { view ->
    view / columns << {
      'de.fspengler.hudson.pview.ConsoleViewColumn'(plugin: 'hudson-pview-plugin@1.8')
      'hudson.plugins.favorite.column.FavoriteColumn'(plugin: 'favorite@1.16')
    }
  }
}

def createFitNessePipelineJob(String jobName, String branch, String targetVersion) {
  pipelineJob(jobName) {
    parameters {
      stringParam('BRANCH', branch, 'Branch from appianps/ps-ext-FitNesseForAppian that is checked out and tested')
      stringParam('TARGET_VERSION', targetVersion, 'Version of Appian for FitNesse to test against, should be given as 17.4, 17.3, etc.')
    }
    definition {
      cpsScm {
        scm {
          git {
            remote {
              url('git@github.com:appianps/ps-ext-FitNesseForAppian')
            }
            branches('${BRANCH}')
          }
        }
        scriptPath('Jenkinsfile')
      }
    }
  }
}
