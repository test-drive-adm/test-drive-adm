#!/bin/bash

CURRENT_DIR=`dirname $0`

# Requires running "mvn clean" and "mvn package [-DskipTests]"
# in order to create a zip in the target folder, and then the
# created zip must be unzipped. FitNesse should be started from
# that directory.

if [[ $1 == "local" ]]; then
  RESOURCES=$(realpath $CURRENT_DIR/../src/main/resources)
  echo "Setting resources to be $RESOURCES"
  echo "To change this for running fitnesse tests, unzip Fitnesse into the target folder, run ./setupCustomPropertiesForMac.sh"
else
  RESOURCES=$(realpath "$CURRENT_DIR/../target/FitNesseForAppian")
  echo "Setting resources to be $RESOURCES"
  echo "To change this for running java tests, run ./setupCustomPropertiesForMac.sh local"
fi
TARGET=$(realpath "$CURRENT_DIR/../target")
DRIVERS="$RESOURCES/lib/drivers"
CUSTOM_PROPERTIES="$RESOURCES/configs/custom.properties"

function sed-populate-key {
  sed -i '' "s#$1=.*#$1=$2#" $CUSTOM_PROPERTIES
}

sed-populate-key "automated.testing.home" "$RESOURCES"
sed-populate-key "download.directory" "$TARGET"
sed-populate-key "chrome.driver.home" "$DRIVERS/chromedriver-mac"
sed-populate-key "firefox.driver.home" "$DRIVERS/geckodriver-mac"
