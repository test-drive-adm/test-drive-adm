#!/usr/bin/bash
if [ -z $1 ]; then
  echo "Site not provided"
  exit 1
fi

site_hostname="$1.appianci.net"
initial_wait_time=20m
wait_delay=1m
retry_attempt=1
total_retries=100

echo "Sites typically take at least 20 minutes to start up; waiting for $initial_wait_time, \
then polling every $wait_delay, $total_retries times"
echo "Waiting for $initial_wait_time"
sleep $initial_wait_time

echo "Begin polling every $wait_delay, $total_retries times"
while [ $retry_attempt -le $total_retries ]; do
  echo "Waiting for site https://$site_hostname/suite/ to come up, attempt $retry_attempt of $total_retries"
  ssh -o StrictHostKeyChecking=no appian@$site_hostname "grep -Fq 'Site configuration completed successfully' /var/log/site-startup"

  if [ $? -eq 0 ]; then
    echo "Site https://$site_hostname/suite/ is up"
    exit 0
  else
    let retry_attempt+=1
  fi

  sleep $wait_delay
done

echo "Site https://$site_hostname/suite/ never came up, failed to deploy objects"
exit 1
