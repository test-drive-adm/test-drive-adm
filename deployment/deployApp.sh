#!/usr/bin/bash
AE_PLUGIN_HOME='/usr/local/appian/ae/_admin/plugins'

verify_plugin_installed() {
  ssh -qoStrictHostKeyChecking=no appian@$1.appianci.net "test -e $AE_PLUGIN_HOME/$2" && echo 0 || echo 1
}

scp_plugin() {
  scp -qoStrictHostKeyChecking=no $2 appian@$1.appianci.net:$AE_PLUGIN_HOME
}

if [ -z $1 ]; then
  echo "Please specify a site name"
  exit 1
fi

SITENAME=$1
echo "Begin deploying site"
echo "Site: https://$SITENAME.appianci.net/suite/"

if [ -z $2 ]; then
  read -p "Please enter the version: " inner_version
  VERSION=$inner_version
else
  VERSION=$2
fi

CURRENT_DIR=`dirname $0`
APP_DIR="$CURRENT_DIR/../apps"
WEB_PLUGIN_NAME="WebAndXMLExtensions-5.4.3.jar"
ADM_PLUGIN_NAME="appian-adm-deployment-2.4.3-SNAPSHOT.jar"
WEB_PLUGIN_LOCATION="$APP_DIR/$WEB_PLUGIN_NAME"
ADM_PLUGIN_LOCATION="$APP_DIR/$ADM_PLUGIN_NAME"

# SCP WebAndXMLExtensionsPlugin if not installed
pluginInstalled=$(verify_plugin_installed $SITENAME $WEB_PLUGIN_NAME)
if [ $pluginInstalled -eq 1 ]; then
  echo "WebAndXMLExtensionsPlugin is not on site, SCPing up"
  scp_plugin $SITENAME $WEB_PLUGIN_LOCATION
fi

# SCP ADM Deployment if not installed
pluginInstalled=$(verify_plugin_installed $SITENAME $ADM_PLUGIN_NAME)
if [ $pluginInstalled -eq 1 ]; then
  echo "ADM plugin is not on the site, SCPing up"
  scp_plugin $SITENAME $ADM_PLUGIN_LOCATION

  echo "Waiting until plugin is installed"
  if (( ${VERSION/./} < 183 )); then
    echo "Version is < 18.3, checking JBoss logs"
    ssh -q appian@$SITENAME.appianci.net 'tail -f /usr/local/appian/ae/logs/jboss1-stdOut.log.* | \
      while read LOGLINE; do [[ "${LOGLINE}" == *"Automated Import"* ]] && pkill -P $$ tail; done'
  fi

  if (( ${VERSION/./} >= 183 )); then
    echo "Version is >= 18.3, checking Tomcat logs"
    ssh -q appian@$SITENAME.appianci.net 'tail -f /usr/local/appian/ae/logs/tomcat-stdOut.log.* | \
      while read LOGLINE; do [[ "${LOGLINE}" == *"Automated Import"* ]] && pkill -P $$ tail; done'
  fi

  echo "Plugin is installed"
fi

ZIP_LOCATION="$APP_DIR/Automated Testing - $VERSION.zip"
ADM_PLUGIN_CLIENT_LOCATION=$(basename $(echo $ADM_PLUGIN_NAME | sed 's#appian-adm-deployment#appian-adm-deployment-client#') .jar)

# Deploy the application
$CURRENT_DIR/$ADM_PLUGIN_CLIENT_LOCATION/deploy-application.sh -url https://$SITENAME.appianci.net/suite \
-ddl_ds jdbc/AppianAnywhere -ddl_path $CURRENT_DIR/AUT_DS_DSL.sql -application_path "$ZIP_LOCATION" \
-username Administrator -password ineedtoadminister
